## **openGauss兼容Oralce存储过程的语法**

- ### 项目内容

  Oracle采用plsql语言规则实现存储过程，openGauss的存储过程语句则是基于plpgsql，是在plsql基础之上进行修改。

  所以虽然存储过程定义语句结构相似，但是opengauss并不是完全兼容oracle语句，所以要对oracle存储过程进行改写

  让其能够适应opengauss，改写内容包括多个方面，如数据类型、命名规则、系统函数调用、函数兼容问题、关键字是

  否保留、执行语句等。

  该项目旨在设计实现oracle存储过程向opengauss存储过程语句的转换。并能通过用户提供的oracle数据库地址、用户、

  模式等信息连接oracle数据库，自动扫描oracle数据库模式下定义的存储过程，之后调用转换解释，实现自动转换。

  

  **语言** :  程序基于**perl**语言，perl语言对于正则表示式匹配替换等功能十分强大，正适合这个项目。

- ### 项目结构

  —resource

  ​     ——plsql                                  #  oracle 存储过程语句

  —src

  ​    ——execute_fun1.pl          # 执行部分,读取配置文件，从文件中读取procedure语句并转换
  
  ​    ——execute_fun2.pl          # 执行部分,读取配置文件，连接数据库,从数据中查询procedure并转换成plpgsql格式

  ​    ——Transformation.pm       # 主程序入口，包括对oracle存储过程语句分割，类型转换等功能

  ​    ——OracleCon.pm                # oracle数据库连接模块

  ​    ——OracleConfig.pm             #  配置文件，包括判断连接数据库还是从文件读取，以及一些存储文件路径、oracle数据库地址等信息

  ​    ——RewriteUtils.pm             #  对存储过程语言中的重写

  —test						                # 测试文件夹，包括单元测试模块和系统测试模块

  ​    ——unit_test                           # 单元测试模块




### 存储过程翻译的一些内部逻辑

#### 数据类型转换

|           Oracle数据类型            |    openGauss数据类型     |
| :---------------------------------: | :----------------------: |
|       NUMBER(p,s),p<=38,s>=0        |      NUMBERIC(p,s)       |
|        NUMBER(p,s),p<=38,s<0        |      NUMBERIC(p,0)       |
|          NUMBER(p,0),p<=4           |         SMALLINT         |
|          NUMBER(p,0),p<=9           |         INTEGER          |
|          NUMBER(p,0),p<=18          |          BIGINT          |
|            BINARY_FLOAT             |          FLOAT4          |
|            BINARY_DOUBLE            |      BINARY_DOUBLE       |
|             PLS_INTEFER             |         INTERGER         |
|        CHAR/CHAR(n)/VARCHAR2        |      TEXT(应该不用)      |
|         TIMESTAMP[(p)],p<=6         |        TIMESTAMP         |
| TIMESTAMP[(p)] WITH TIME ZONE ,p<=6 | TIMESTAMP WITH TIME ZONE |
|                LONG                 |           TEXT           |
|              LONG RAW               |          BYTEA           |
|                NCLOB                |           TEXT           |
|                BLOB                 |          BYTEA           |
|                BFILE                |          BYTEA           |
|            ROWID/UROWID             |           OID            |
|               XMLTYPE               |           XML            |
|            SDO_GEOMETRY             |         GEOMETRY         |

#### 函数转换   (左边为oracle中支持， 右边为opengauss中替换版本)

- Systimestamp -> localtimstamp 

- .nextval | .currval  -> nextval( ) | currval ( )

- length -> char_length

- nchr -> chr

- nls_upper/nls_lower -> nlssort

- nvl2(nvl is supported)  nvl2(a,b,c)->decode(a,NULL,c,b)

- trim(* ) -> trim( both *)

- ORA_HASH ->hill_hash_XXX

- raise_application_error->RAISE EXCEPTION '.....' (USING ERRCODE= '') 

- SQLCODE->SQLSTATE

- xmltype-> xmlparse(DOCUMENT,convert_from())

- dup_val_on_index->unique_violation

- XMLELEMENT(*) ->   XMLELEMENT(name *)

- replace(a,b) -> replace(a,b,'')

- nvl2(nvl is supported)  nvl2(a,b,c)->decode(a,NULL,c,b)

- DBMS_LOB.GETLENGTH->octet_length

- DBMS_LOB.SUBSTR->substr

- DBTIMEZONE ->PG_TIMEZONE_NAMES

- binary_float_nan| binary_double_nan -> nan

- 代替了 open语法 

  open cursor_var for   -> open cursor_var for execute

- SQLCODE->SQLSTATE

- INSERTING|DELETING|UPDATING -> TG_OP = 'INSERT'|'DELETE'|'UPDATE'  (修改触发器的内置函数)

- ON COMMIT * DEFINITION -> ON COMMIT *

- UTL_RAW.CAST_TO_RAW->encode

- UTL_MATCH.EDIT_DISTANCE->levenshtein

- replace %rowtype

- open cursor_var for   -> open cursor_var for execute

- SYS_REFCURSOR -> ref cursor

- systimestamp -> pg_systimestamp()

- to_nchar->ranslate()

- TO_TIMESTAMP_TZ  -> timestamp with time zone 

- 移除了to_clob()

- 移除了sys.

- 移除了mdsys

- 移除了DBMS_OUTPUT.ENABLE

- 移除了cursor (i in int)              in is default

- PERCENT_RANK   opengauss和oracle中都存在该函数，但是opengauss中不接受参数，相同的还有cume_dist.

  

  #### 细则内容
  
  由于opengauss中不一定存在oracle中存储的schema，所以对存储过程名加上 " " 防止命名错误。
  
  #### oracle数据库连接
  
  连接oracle数据库查询指定schema下的所有存储过程，将其导出调用转换模块实现语法翻译。
  
  需要提供host、sid、username、password等字段
  
  sid可以通过以下语句查看
  
  ```sql
  select instance_name from  V$instance;
  ```
  
  连接到数据库后通过一下语句完成存储过程查询
  
  ```sql
  select name,line,text from all_source where owner = '$username' and type = 'PROCEDURE'  ;
  ```
  
  
