sub convert_body_plsql{
    my ($self,$plsql_body) = @_;
    return if ($plsql_body =~ /^[\s\t\n]*$/);

	################### below is some rules to rewrite ############
	# for example : rewrite variable name which is illegeal in opengauss 
	# remove or replace some functions that are unsupport in opengauss (string_agg....)  etc.

	# first remove some extra space between operators
	$plsql_body =~ s/=\s+>/=>/gs;
	$plsql_body =~ s/<\s+=/<=/gs;
	$plsql_body =~ s/>\s+=/>=/gs;
	$plsql_body =~ s/!\s+=/!=/gs;
	$plsql_body =~ s/<\s+>/<>/gs;
	$plsql_body =~ s/:\s+=/:=/gs;
	$plsql_body =~ s/\|\s+\|/\|\|/igs;
	$plsql_body =~ s/!=([+\-])/!= $1/gs;

    # remove sys 
	$plsql_body =~ s/\bSYS\.//igs;
	# replace systimestamp 
	$plsql_body =~ s/\bSYSTIMESTAMP\b/LOCALTIMESTAMP/igs;

	# replace EXEC function into variable, ex: EXEC :a := test(:r,1,2,3);
	$plsql_body =~ s/\bEXEC\s+:([^\s:]+)\s*:=/SELECT INTO $2/igs;

	# replace simple EXEC function call by SELECT function
	$plsql_body =~ s/\bEXEC(\s+)/SELECT$1/igs;

	# INSERTING|DELETING|UPDATING -> TG_OP = 'INSERT'|'DELETE'|'UPDATE'
	$plsql_body =~ s/\bINSERTING\b/TG_OP = 'INSERT'/igs;
	$plsql_body =~ s/\bDELETING\b/TG_OP = 'DELETE'/igs;
	$plsql_body =~ s/\bUPDATING\b/TG_OP = 'UPDATE'/igs;

	# replace on commit ** definition
	$plsql_body =~ s/ON\s+COMMIT\s+PRESERVE\s+DEFINITION/ON COMMIT PRESERVE ROWS/igs;
	$plsql_body =~ s/ON\s+COMMIT\s+DROP\s+DEFINITION/ON COMMIT DROP/igs;

	my $conv_current_time = 'clock_timestamp()';
	# Replace sysdate +/- N by localtimestamp - 1 day intervel
	$plsql_body =~ s/\bSYSDATE\s*(\+|\-)\s*(\d+)/$conv_current_time $1 interval '$2 days'/igs;
    # convert array   arr(i) to arr[i]
    $plsql_body =~ s/\b([a-z0-9_]+)\(([^\(\)]+)\)(\.[a-z0-9_]+)/$1\[$2\]$3/igs;


	# remove/replace some function
	# nextval is different from oracle's to opengauss's   
    # oracle just like ***.nextval  opengauss : nextval('***'). 
    $plsql_body =~ s/\s*\b(\w+)\.nextval/nextval('\L$1\E')/isg;
    $plsql_body =~ s/\s*\b(\w+)\.currval/currval('\L$1\E')/isg;
	# Try to fix call to string_agg with a single argument (allowed in oracle)
	$plsql_body =~ s/\b	\(([^,\(\)]+)\s+(ORDER\s+BY)/string_agg($1, '' $2/igs;
	# remove to_clob();
	$plsql_body =~ s/TO_CLOB\s*\(/\(/igs;
	# replace minus
	$plsql_body =~ s/\bMINUS\b/EXCEPT/igs;
	# Replace call to trim into btrim
	$plsql_body =~ s/\bTRIM\s*\(([^\(\)]+)\)/trim(both $1)/igs;
	# replace XMLTYPE function
	$plsql_body =~ s/XMLTYPE\s*\(\s*([^,]+)\s*,[^\)]+\s*\)/xmlparse(DOCUMENT, convert_from($1, 'utf-8'))/igs;
	$plsql_body =~ s/XMLTYPE\.CREATEXML\s*\(\s*[^\)]+\s*\)/xmlparse(DOCUMENT, convert_from($1, 'utf-8'))/igs;
	# rewrite replace(a,b) with three argument
	$plsql_body =~ s/REPLACE\s*\((.*?),(.*?)\)/replace($1, $2, '')/is;
	# replace DEFAULT empty_blob() and empty_clob()
	my $empty = 'NULL' ;
	$plsql_body =~ s/(empty_blob|empty_clob)\s*\(\s*\)/$empty/is;
	$plsql_body =~ s/(empty_blob|empty_clob)\b/$empty/is;
	# rewrite nvl2
	# nvl is supported in opengauss but nvl2 isn't, so replace nvl2 by decode
	$plsql_body =~ s/NVL2\s*\((.*?),(.*?),(.*?)\)/DECODE \($1,NULL,$3,$2\)/isg;


	# comment DBMS_OUTPUT.ENABLE calls
	$plsql_body =~ s/(DBMS_OUTPUT.ENABLE[^;]+;)/-- $1/isg;
	# DBMS_LOB.GETLENGTH can be replaced by binary length.
	$plsql_body =~ s/DBMS_LOB.GETLENGTH/octet_length/igs;
	# DBMS_LOB.SUBSTR can be replaced by SUBSTR()
	$plsql_body =~ s/DBMS_LOB.SUBSTR/substr/igs;
	
	# replace special IEEE 754 values for not a number and infinity
	$plsql_body =~ s/BINARY_(FLOAT|DOUBLE)_NAN/'NaN'/igs;
	$plsql_body =~ s/([\-]*)BINARY_(FLOAT|DOUBLE)_INFINITY/'$1Infinity'/igs;
	$plsql_body =~ s/'([\-]*)Inf'/'$1Infinity'/igs;

	# replace PIPE ROW by RETURN NEXT
	$plsql_body =~ s/PIPE\s+ROW\s*/RETURN NEXT /igs;
	$plsql_body =~ s/(RETURN NEXT )\(([^\)]+)\)/$1$2/igs;

    # replace the UTC convertion with the PG syntaxe
	$plsql_body =~ s/SYS_EXTRACT_UTC\s*\(([^\)]+)\)/($1 AT TIME ZONE 'UTC')/is;
	# remove call to XMLCDATA, there's no such function with PostgreSQL
	$plsql_body =~ s/XMLCDATA\s*\(([^\)]+)\)/'<![CDATA[' || $1 || ']]>'/is;
	# remove call to getClobVal() or getStringVal, no need of that
	$plsql_body =~ s/\.(GETCLOBVAL|GETSTRINGVAL|GETNUMBERVAL|GETBLOBVAL)\s*\(\s*\)//is;
	# add the name keyword to XMLELEMENT
	$plsql_body =~ s/XMLELEMENT\s*\(\s*/XMLELEMENT(name /is;


	# dup_val_on_index => unique_violation : already exist exception
	$plsql_body =~ s/\bdup_val_on_index\b/unique_violation/igs;

	# replace exception   
    # reaise_application_error -> raise exception 
    # two ways to show exception   here choose the one that doesn't contain error code

    $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+),\s*(true|false)\s*\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . ";"/iges;
	$plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+)\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . ";"/iges;
	$plsql_body =~ s/DBMS_STANDARD\.RAISE EXCEPTION/RAISE EXCEPTION/igs;

    # for the reason that i doesn't find  the error code in opengauss (maybe) 
	# $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+),\s*(true|false)\s*\)\s*;/"RAISE EXCEPTION '%', " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	# $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+)\)\s*;/"RAISE EXCEPTION '%', " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	# $plsql_body =~ s/DBMS_STANDARD\.RAISE EXCEPTION/RAISE EXCEPTION/igs;


	# rewrite having group by
	$plsql_body =~ s/\bHAVING\b((?:(?!SELECT|INSERT|UPDATE|DELETE|WHERE|FROM).)*?)\bGROUP BY\b((?:(?!SELECT|INSERT|UPDATE|DELETE|WHERE|FROM).)*?)((?=UNION|ORDER BY|LIMIT|INTO |FOR UPDATE|PROCEDURE|\)\s+(?:AS)*[a-z0-9_]+\s+)|$)/GROUP BY$2 HAVING$1/gis;

	# add strict 
	$plsql_body =~ s/\b(SELECT\b[^;]*?INTO)(.*?)(EXCEPTION.*?(?:NO_DATA_FOUND|TOO_MANY_ROW))/$1 STRICT $2 $3/igs;
	$plsql_body =~ s/\b((?:SELECT|EXECUTE)\s+[^;]*?\s+INTO)(\s+(?!STRICT))/$1 STRICT$2/igs;
	$plsql_body =~ s/(INSERT\s+INTO\s+)STRICT\s+/$1/igs;

	# remove some extra chars at the end of keywords
	$plsql_body =~ s/\b(END\s*[^;\s]+\s*(?:;|$))/rewrite_end($1)/iges;

	# rewrite comment in CASE between WHEN and THEN
	$plsql_body =~ s/(\s*)(WHEN\s+[^\s]+\s*)(\%ORA2PG_COMMENT\d+\%)(\s*THEN)/$1$3$1$2$4/igs;

	# sqlcode -> sqlstate
	$plsql_body =~ s/\bSQLCODE\b/SQLSTATE/igs;
	# remove MSSYS.
	$plsql_body =~ s/\bMDSYS\.//igs;
	# rewrite reverse
	$plsql_body =~ s/\bFOR(.*?)IN\s+REVERSE\s+([^\.\s]+)\s*\.\.\s*([^\s]+)/FOR$1IN REVERSE $3..$2/isg;

	# replace exit at end of cursor
	$plsql_body =~ s/EXIT\s+WHEN\s+([^\%;]+)\%\s*NOTFOUND\s*;/EXIT WHEN NOT FOUND; \/\* apply on $1 \*\//isg;
	$plsql_body =~ s/EXIT\s+WHEN\s+\(\s*([^\%;]+)\%\s*NOTFOUND\s*\)\s*;/EXIT WHEN NOT FOUND;  \/\* apply on $1 \*\//isg;

	$plsql_body =~ s/EXIT\s+WHEN\s+([^\%;]+)\%\s*NOTFOUND\s+([^;]+);/EXIT WHEN NOT FOUND $2;  \/\* apply on $1 \*\//isg;
	$plsql_body =~ s/EXIT\s+WHEN\s+\(\s*([^\%;]+)\%\s*NOTFOUND\s+([^\)]+)\)\s*;/EXIT WHEN NOT FOUND $2;  \/\* apply on $1 \*\//isg;
	
	# replace UTL_MATH function by fuzzymatch function
	$plsql_body =~ s/UTL_MATCH.EDIT_DISTANCE/levenshtein/igs;

    # replace UTL_ROW.CAST_TO_RAW function by encode function
    $plsql_body =~ s/UTL_RAW.CAST_TO_RAW\s*\(\s*([^\)]+)\s*\)/encode($1::bytea, 'hex')::bytea/igs;

	# replace illegal outerjoin by (+)
	$plsql_body =~ s/\%OUTERJOIN\d+\%/\(\+\)/igs;

	# replace <> null -> is not null 
	$plsql_body =~ s/\s*(<>|\!=)\s*NULL/ IS NOT NULL/igs;
	
	# $plsql_body =~ s/(?!:)(.)=\s*NULL/$1 IS NULL/igs;

	# add from 
	$plsql_body =~ s/(\bDELETE\s+)(?!FROM|WHERE|RESTRICT|CASCADE|NO ACTION)\b/$1FROM /igs;


	# Oracle doesn't require parenthesis after VALUES, PostgreSQL has
	# similar proprietary syntax but parenthesis are mandatory
	$plsql_body =~ s/(INSERT\s+INTO\s+(?:.*?)\s+VALUES\s+)([^\(\)\s]+)\s*;/$1\($2.*\);/igs;

	# replace some windows function issues with KEEP (DENSE_RANK FIRST ORDER BY ...)
	$plsql_body =~ s/\b(MIN|MAX|SUM|AVG|COUNT|VARIANCE|STDDEV)\s*\(([^\)]+)\)\s+KEEP\s*\(DENSE_RANK\s+(FIRST|LAST)\s+(ORDER\s+BY\s+[^\)]+)\)\s*(OVER\s*\(PARTITION\s+BY\s+[^\)]+)\)/$3_VALUE($2) $5 $4)/igs;

	$plsql_body =~ s/TIMESTAMP\s*('[^']+')/$1/igs;

	my $tmp_code = $plsql_body;
	while ($tmp_code =~ s/\bFOR\s+([^\s]+)\s+IN(.*?)LOOP//is)
	{
		my $varname = $1;
		my $clause = $2;
		my @code = split(/\bBEGIN\b/i, $plsql_body);
		if ($code[0] !~ /\bDECLARE\s+.*\b$varname\s+/is)
		{
			# When the cursor is refereing to a statement, declare
			# it as record otherwise it don't need to be replaced
			if ($clause =~ /\bSELECT\b/is)
			{
				# append variable declaration to declare section
				if ($plsql_body !~ s/\bDECLARE\b/DECLARE\n  $varname RECORD;/is)
				{
					# No declare section
					$plsql_body = "DECLARE\n  $varname RECORD;\n" . $plsql_body;
				}
			}
		}
	}
    return $plsql_body;

}


my @plsql = {
"    IF v_version IS NULL THEN
        RETURN v_name;
    END IF;
    RETURN v_name || '/' || v_version;
END;" , 
"    FOR referrer_key IN referrer_keys LOOP
        func_cmd := func_cmd ||
          ' IF v_' || referrer_key.kind
          || ' LIKE ''' || referrer_key.key_string
          || ''' THEN RETURN ''' || referrer_key.referrer_type
          || '''; END IF;';
    END LOOP;" ,
"    IF a_pos1 = 0 THEN
        RETURN;
    END IF;
    a_pos2 := instr(v_url, '/', a_pos1 + 2);
    IF a_pos2 = 0 THEN
        v_host := substr(v_url, a_pos1 + 2);
        v_path := '/';
        RETURN;
    END IF;" , 
"select nvl2(1, sysdate-(sysdate-1/24/60), sysdate) from dual;
select nvl2(null,sysdate-(sysdate-1/24/60), sysdate) from dual;" , 

"sctx := t_string_agg(NULL); 
   RETURN ODCIConst.Success; 
 END; 
 MEMBER FUNCTION ODCIAggregateIterate(self IN OUT t_string_agg,value IN VARCHAR2 ) 
   RETURN NUMBER IS 
 BEGIN 
   SELF.g_string := self.g_string || ',' || value; 
   RETURN ODCIConst.Success; 
 END; " , 
"SELECT XMLELEMENT(\"TEST\",
                  XMLELEMENT(\"AA\",
                             XMLELEMENT(\"BB\", 'XXX'),
                             XMLELEMENT(\"CC\", 'XXX')))
  FROM DUAL;
  SELECT XMLELEMENT(\"test\")
    FROM DUAL"
};

foreach my $key (@plsql) {
  print "convert result:"."\n".convert_body_plsql($key)."\n";  
} 