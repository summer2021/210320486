
=head1  convert_cursor

    useages: convert args in plsql .
        change in out ,type ....

=cut

sub convert_cursor
{
	my $str = shift;

	# remove cursor (i in int)  in is default
	while ($str =~ s/(\bCURSOR\b[^\(]+)\(([^\)]+\bIN\b[^\)]+)\)/$1\(\%\%CURSORREPLACE\%\%\)/is) {
		my $args = $2;
		$args =~ s/\bIN\b//igs;
		$str =~ s/\%\%CURSORREPLACE\%\%/$args/is;
	}

	# replace %rowtype
	$str =~ s/\bTYPE\s+([^\s]+)\s+(IS\s+REF\s+CURSOR|REFCURSOR)\s+RETURN\s+[^\s\%]+\%ROWTYPE;/$1 REF CURSOR;/isg;

	# replace sys_refcursor
	$str =~ s/\bSYS_REFCURSOR\b/REF CURSOR/isg;

	# replace open 
	$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)((?:[^;]+?)USING)/$1 EXECUTE$2/isg;
	#$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)\s+((?!EXECUTE)(?:[^;]+?)\|\|)/$1 EXECUTE $2/isg;
	$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)\s+([^\s]+\s*;)/$1 EXECUTE $2/isg;
	# remove empty parenthesis after an open cursor
	$str =~ s/(OPEN\s+[^\(\s;]+)\s*\(\s*\)/$1/isg;

	# replace is by for (some version for/is is both ok) 
	# while($str =~ s/(\bCURSOR\b\s*(.*)\s*)IS/$1FOR/is){};
    return $str;
}

my @plsql = { 
"cursor cur1 is
select shdh,line_no,order_code,order_line_no,rec_qty,inv_loc,itemcode,id,order_type
from  w_receive_erp
where  w_receive_erp.order_code is not null
and order_code >'P15072009'
and status=1 ;" ,
"DELCARE
CURSOR C_EMP IS SELECT empno,ename,salary
FROM emp
WHERE salary>2000
ORDER BY ename;" ,
"CURSOR c_name IS
SELECT sal FROM emp WHERE empno=7369;
my_sal emp.sal%TYPE;"
};


foreach my $key (@plsql) {
  print "convert result:"."\n".convert_cursor($key)."\n";  
} 