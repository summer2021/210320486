
sub set_error_code
{
	my $code = shift;

	my $orig_code = $code;

	$code =~ s/-20(\d{3})/'45$1'/;
	if ($code =~ s/-20(\d{2})/'450$1'/ || $code =~ s/-20(\d{1})/'4500$1'/) {
		print STDERR "WARNING: exception code has less than 5 digit, proceeding to automatic adjustement.\n";
		$code .= " /* code was: $orig_code */";
	}

	return $code;
}

# Fix case where the raise_application_error() parameters are named by removing them
sub remove_named_parameters
{
	my $str = shift;

	$str =~ s/\w+\s*=>\s*//g;

	return $str;
}
	my $str = "raise_application_error(-20000,
                 'Unable to create a new job: a job is currently running.');";
	$str =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+),\s*(true|false)\s*\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	$str =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+)\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	$str =~ s/DBMS_STANDARD\.RAISE EXCEPTION/RAISE EXCEPTION/igs;

	print "$str";