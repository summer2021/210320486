sub rewrite_end
{
	my $str = shift;

	if ($str !~ /(END\b\s*)(IF\b|LOOP\b|CASE\b|INTO\b|FROM\b|END\b|ELSE\b|AND\b|OR\b|WHEN\b|AS\b|,|\)|\(|\||[<>=]|NOT LIKE|LIKE|WHERE|GROUP|ORDER)/is) {
		$str =~ s/(END\b\s*)[\w"\.]+\s*(?:;|$)/$1;/is;
	}

	return $str;
}

my $error_str = " BEGIN 
	vertex1 int default  5;
	END dede; 
	\$\$ language plpgsql";
my $str = " BEGIN 
	vertex1 int default 5;
	END dede; 
	\$\$ language plpgsql";
print "filter_str:".rewrite_end($str)."\n";
print "filter_str:".rewrite_end($str)."\n";