CREATE OR REPLACE PROCEDURE cs_parse_url(  
    v_url IN VARCHAR2,  
    v_host OUT VARCHAR2,  -- This will be passed back  
    v_path OUT VARCHAR2,  -- This one too  
    v_query OUT VARCHAR2) -- And this one  
IS  
    a_pos1 INTEGER;  
    a_pos2 INTEGER;  
BEGIN  
    v_host := NULL;  
    v_path := NULL;  
    v_query := NULL;  
    a_pos1 := instr(v_url, '//');  
 
    IF a_pos1 = 0 THEN  
        RETURN;  
    END IF;  
    a_pos2 := instr(v_url, '/', a_pos1 + 2);  
    IF a_pos2 = 0 THEN  
        v_host := substr(v_url, a_pos1 + 2);  
        v_path := '/';  
        RETURN;  
END IF;  

    v_host := substr(v_url, a_pos1 + 2, a_pos2 - a_pos1 - 2);  
    a_pos1 := instr(v_url, '?', a_pos2 + 1);  
 
    IF a_pos1 = 0 THEN  
        v_path := substr(v_url, a_pos2);  
        RETURN;  
    END IF;  
 
    v_path := substr(v_url, a_pos2, a_pos1 - a_pos2);  
    v_query := substr(v_url, a_pos1 + 1);  
END;  