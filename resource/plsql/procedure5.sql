create or replace procedure scott.p_procedure_demo(i_a in number,i_b in number,o_result out number,io_flag in out varchar2) is
  v_message varchar2(30) := '存储过程模板';
begin
  io_flag := 'Y';
  -- i_b = 0 时，可验证报错
  o_result := i_a / i_b;

  -- 格式调整  1/2 = .5 => 0.5
  o_result := regexp_replace(o_result, '^\.', '0.');
exception
  when others then
    io_flag := 'N';
    dbms_output.put_line(sqlcode || ', ' || sqlerrm);
    dbms_output.put_line(dbms_utility.format_error_backtrace);
end;
