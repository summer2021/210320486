create or replace procedure test() as 
    Cursor cursor is select name from student; name varchar(20); 
    begin 
       for name in cursor LOOP    --开始循环
         begin 
           dbms_output.putline(name); 
         end; 
       end LOOP; 
end test;
