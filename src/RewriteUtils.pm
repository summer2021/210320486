package RewriteUtils;

use strict;


=head1  rewrite_connect_by
	
	connect by is only supported in oracle . for opengauss ,use WITH RECURSIVE replace connect by 

=cut

sub rewrite_connect_by
{
	my ($class, $str) = @_;
    my @statements;
	return $str if ($str !~ /\bCONNECT\s+BY\b/is);
	$str =~ s/\s*([^\s]*)/$1/;
	my $final_query = "WITH RECURSIVE cte AS (\n";

	# Remove NOCYCLE, not supported at now
	$str =~ s/\s+NOCYCLE//is;

	# Remove SIBLINGS keywords and enable siblings rewrite 
	my $siblings = 0;
	if ($str =~ s/\s+SIBLINGS//is) {
		$siblings = 1;
	}

	# Extract UNION part of the query to past it at end
	my $union = '';
	if ($str =~ s/(CONNECT BY.*)(\s+UNION\s+.*)/$1/is) {
		$union = $2;
	}
	
	# Extract order by to past it to the query at end
	my $order_by = '';
	if ($str =~ s/\s+ORDER BY(.*)//is) {
		$order_by = $1;
	}

	# Extract group by to past it to the query at end
	my $group_by = '';
	if ($str =~ s/(\s+GROUP BY.*)//is) {
		$group_by = $1;
	}

	# Extract the starting node or level of the tree 
	my $where_clause = '';
	my $start_with = '';
	if ($str =~ s/WHERE\s+(.*?)\s+START\s+WITH\s*(.*?)\s+CONNECT BY\s*//is) {
		$where_clause = " WHERE $1";
		$start_with = $2;
	} elsif ($str =~ s/WHERE\s+(.*?)\s+CONNECT BY\s+(.*?)\s+START\s+WITH\s*(.*)/$2/is) {
		$where_clause = " WHERE $1";
		$start_with = $3;
	} elsif ($str =~ s/START\s+WITH\s*(.*?)\s+CONNECT BY\s*//is) {
		$start_with = $1;
	} elsif ($str =~ s/\s+CONNECT BY\s+(.*?)\s+START\s+WITH\s*(.*)/ $1 /is) {
		$start_with = $2;
	} else {
		$str =~ s/CONNECT BY\s*//is;
	}

	# remove alias from where clause
	$where_clause =~ s/\b[^\.]\.([^\s]+)\b/$1/gs;

	# Extract the CONNECT BY clause in the hierarchical query
	my $prior_str = '';
	my @prior_clause = '';
	if ($str =~ s/([^\s]+\s*=\s*PRIOR\s+.*)//is) {
		$prior_str =  $1;
	} elsif ($str =~ s/(\s*PRIOR\s+.*)//is) {
		$prior_str =  $1;
	} else {
		# look inside subqueries if we have a prior clause
		my @ids = $str =~ /\%SUBQUERY(\d+)\%/g;
		my $sub_prior_str = '';
		foreach my $i (@ids) {
			if ($class->{sub_parts}{$i} =~ s/([^\s]+\s*=\s*PRIOR\s+.*)//is) {
				$sub_prior_str =  $1;
				$str =~ s/\%SUBQUERY$i\%//;
			} elsif ($class->{sub_parts}{$i} =~ s/(\s*PRIOR\s+.*)//is) {
				$sub_prior_str =  $1;
				$str =~ s/\%SUBQUERY$i\%//;
			}
			$sub_prior_str =~ s/^\(//;
			$sub_prior_str =~ s/\)$//;
			($prior_str ne '' || $sub_prior_str eq '') ? $prior_str .= ' ' . $sub_prior_str : $prior_str = $sub_prior_str;
		}
	}
	if ($prior_str) {
		# Try to extract the prior clauses
		my @tmp_prior = split(/\s*AND\s*/, $prior_str);
		$tmp_prior[-1] =~ s/\s*;\s*//s;
		my @tmp_prior2 = ();
		foreach my $p (@tmp_prior) {
			if ($p =~ /\bPRIOR\b/is) {
				push(@prior_clause, split(/\s*=\s*/i, $p));
			} else {
				$where_clause .= " AND $p";
			}
		}
		if ($siblings) {
			if ($prior_clause[-1] !~ /PRIOR/i) {
				$siblings = $prior_clause[-1];
			} else {
				$siblings = $prior_clause[-2];
			}
			$siblings =~ s/\s+//g;
		}
		shift(@prior_clause) if ($prior_clause[0] eq '');
		my @rebuild_prior = ();
		# Place PRIOR in the left part if necessary
		for (my $i = 0; $i < $#prior_clause; $i+=2) {
			if ($prior_clause[$i+1] =~ /PRIOR\s+/i) {
				my $tmp = $prior_clause[$i];
				$prior_clause[$i] = $prior_clause[$i+1];
				$prior_clause[$i+1] = $tmp;
			}
			push(@rebuild_prior, "$prior_clause[$i] = $prior_clause[$i+1]");
		}
		@prior_clause = @rebuild_prior;
		# Remove table aliases from prior clause
		map { s/\s*PRIOR\s*//s; s/[^\s\.=<>!]+\.//s; } @prior_clause;
	}
	my $bkup_query = $str;
	# Construct the initialization query
	$str =~ s/(SELECT\s+)(.*?)(\s+FROM)/$1COLUMN_ALIAS$3/is;
	my @columns = split(/\s*,\s*/, $2);
	# When the pseudo column LEVEL is used in the where clause
	# and not used in columns list, add the pseudo column
	if ($where_clause =~ /\bLEVEL\b/is && !grep(/\bLEVEL\b/i, @columns)) {
		push(@columns, 'level');
	}
	my @tabalias = ();
	my %connect_by_path = ();
	for (my $i = 0; $i <= $#columns; $i++) {
		my $found = 0;
		while ($columns[$i] =~ s/\%SUBQUERY(\d+)\%/$class->{sub_parts}{$1}/is) {
			# Get out of here next run when a call to SYS_CONNECT_BY_PATH is found
			# This will prevent opening too much subquery in the function parameters
			last if ($found);
			$found = 1 if ($columns[$i]=~ /SYS_CONNECT_BY_PATH/is);
		};
		# Replace LEVEL call by a counter, there is no direct equivalent in PostgreSQL
		if (lc($columns[$i]) eq 'level') {
			$columns[$i] = "1 as level";
		} elsif ($columns[$i] =~ /\bLEVEL\b/is) {
			$columns[$i] =~ s/\bLEVEL\b/1/is;
		}
		# Replace call to SYS_CONNECT_BY_PATH by the right concatenation string
		if ($columns[$i] =~ s/SYS_CONNECT_BY_PATH\s*[\(]*\s*([^,]+),\s*([^\)]+)\s*\)/$1/is) {
			my $col = $1;
			$connect_by_path{$col}{sep} = $2;
			# get the column alias
			if ($columns[$i] =~ /\s+([^\s]+)\s*$/s) {
				$connect_by_path{$col}{alias} = $1;
			}
		}
		if ($columns[$i] =~ /([^\.]+)\./s) {
			push(@tabalias, $1) if (!grep(/^\Q$1\E$/i, @tabalias));
		}
		extract_subpart($class, \$columns[$i]);

		# Append parenthesis on new subqueries values
		foreach my $z (sort {$a <=> $b } keys %{$class->{sub_parts}}) {
			next if ($class->{sub_parts}{$z} =~ /^\(/);
			# If subpart is not empty after transformation
			if ($class->{sub_parts}{$z} =~ /\S/is) { 
				# add open and closed parenthesis 
				$class->{sub_parts}{$z} = '(' . $class->{sub_parts}{$z} . ')';
			} elsif ($statements[$i] !~ /\s+(WHERE|AND|OR)\s*\%SUBQUERY$z\%/is) {
				# otherwise do not report the empty parenthesis when this is not a function
				$class->{sub_parts}{$z} = '(' . $class->{sub_parts}{$z} . ')';
			}
		}
	}

	# Extraction of the table aliases in the FROM clause
	my $cols = join(',', @columns);
	$str =~ s/COLUMN_ALIAS/$cols/s;
	if ($str =~ s/(\s+FROM\s+)(.*)/$1FROM_CLAUSE/is) {
		my $from_clause = $2;
		$str =~ s/FROM_CLAUSE/$from_clause/;
	}

	# Now append the UNION ALL query that will be called recursively
	$final_query .= $str;
	$final_query .= ' WHERE ' . $start_with . "\n" if ($start_with);
	#$where_clause =~ s/^\s*WHERE\s+/ AND /is;
	#$final_query .= $where_clause . "\n";
	$final_query .= "  UNION ALL\n";
	if ($siblings && !$order_by) {
		$final_query =~ s/(\s+FROM\s+)/,ARRAY[ row_number() OVER (ORDER BY $siblings) ] as hierarchy$1/is;
	} elsif ($siblings) {

		$final_query =~ s/(\s+FROM\s+)/,ARRAY[ row_number() OVER (ORDER BY $order_by) ] as hierarchy$1/is;
	}
	$bkup_query =~ s/(SELECT\s+)(.*?)(\s+FROM)/$1COLUMN_ALIAS$3/is;
	@columns = split(/\s*,\s*/, $2);
	# When the pseudo column LEVEL is used in the where clause
	# and not used in columns list, add the pseudo column
	if ($where_clause =~ /\bLEVEL\b/is && !grep(/\bLEVEL\b/i, @columns)) {
		push(@columns, 'level');
	}
	for (my $i = 0; $i <= $#columns; $i++) {
		my $found = 0;
		while ($columns[$i] =~ s/\%SUBQUERY(\d+)\%/$class->{sub_parts}{$1}/is) {
			# Get out of here when a call to SYS_CONNECT_BY_PATH is found
			# This will prevent opening subquery in the function parameters
			last if ($found);
			$found = 1 if ($columns[$i]=~ /SYS_CONNECT_BY_PATH/is);
		};
		if ($columns[$i] =~ s/SYS_CONNECT_BY_PATH\s*[\(]*\s*([^,]+),\s*([^\)]+)\s*\)/$1/is) {
			$columns[$i] = "c.$connect_by_path{$1}{alias} || $connect_by_path{$1}{sep} || " . $columns[$i];
		}
		if ($columns[$i] !~ s/\b[^\.]+\.LEVEL\b/(c.level+1)/igs) {
			$columns[$i] =~ s/\bLEVEL\b/(c.level+1)/igs;
		}
		extract_subpart($class, \$columns[$i]);

		# Append parenthesis on new subqueries values
		foreach my $z (sort {$a <=> $b } keys %{$class->{sub_parts}}) {
			next if ($class->{sub_parts}{$z} =~ /^\(/);
			# If subpart is not empty after transformation
			if ($class->{sub_parts}{$z} =~ /\S/is) { 
				# add open and closed parenthesis 
				$class->{sub_parts}{$z} = '(' . $class->{sub_parts}{$z} . ')';
			} elsif ($statements[$i] !~ /\s+(WHERE|AND|OR)\s*\%SUBQUERY$z\%/is) {
				# otherwise do not report the empty parenthesis when this is not a function
				$class->{sub_parts}{$z} = '(' . $class->{sub_parts}{$z} . ')';
			}
		}
	}
	$cols = join(',', @columns);
	$bkup_query =~ s/COLUMN_ALIAS/$cols/s;
	my $prior_alias = '';
	if ($bkup_query =~ s/(\s+FROM\s+)(.*)/$1FROM_CLAUSE/is) {
		my $from_clause = $2;
		if ($from_clause =~ /\b[^\s]+\s+(?:AS\s+)?([^\s]+)\b/) {
			my $a = $1;
			$prior_alias = "$a." if (!grep(/\b$a\.[^\s]+$/, @prior_clause));
		}
		$bkup_query =~ s/FROM_CLAUSE/$from_clause/;
	}

	# Remove last subquery alias in the from clause to put our own 
	$bkup_query =~ s/(\%SUBQUERY\d+\%)\s+[^\s]+\s*$/$1/is;
	if ($siblings && $order_by) {
		$bkup_query =~ s/(\s+FROM\s+)/, array_append(c.hierarchy, row_number() OVER (ORDER BY $order_by))  as hierarchy$1/is;
	} elsif ($siblings) {
		$bkup_query =~ s/(\s+FROM\s+)/, array_append(c.hierarchy, row_number() OVER (ORDER BY $siblings))  as hierarchy$1/is;
	}
	$final_query .= $bkup_query;
	map { s/^\s*(.*?)(=\s*)(.*)/c\.$1$2$prior_alias$3/s; } @prior_clause;
	$final_query .= " JOIN cte c ON (" . join(' AND ', @prior_clause) . ")\n";
	if ($siblings) {
		$order_by = " ORDER BY hierarchy";
	} elsif ($order_by) {
		$order_by =~ s/^, //s;
		$order_by = " ORDER BY $order_by";
	}
	$final_query .= "\n) SELECT * FROM cte$where_clause$union$group_by$order_by;\n";

	return $final_query;
}

sub extract_subpart
{
	my ($class, $str) = @_;

	while ($$str =~ s/\(([^\(\)]*)\)/\%SUBQUERY$class->{sub_parts_idx}\%/s) {
		$class->{sub_parts}{$class->{sub_parts_idx}} = $1;
		$class->{sub_parts_idx}++;
	}
	my @done = ();
	foreach my $k (sort { $b <=> $a } %{$class->{sub_parts}}) {
		if ($class->{sub_parts}{$k} =~ /\%OUTERJOIN\d+\%/ && $class->{sub_parts}{$k} !~ /\b(SELECT|FROM|WHERE)\b/i) {
			$$str =~ s/\%SUBQUERY$k\%/\($class->{sub_parts}{$k}\)/s;
			push(@done, $k);
		}
	}
	foreach (@done) {
		delete $class->{sub_parts}{$_};
	}
}


=head2 rewrite_object_name
    checing for naming errors
    if the name is illegal , add double quote ;
=cut


sub rewrite_object_name
{
	my ($self, $obj_name) = @_;

    next if ($obj_name =~ /^SYS_NC\d+/);

    #remove any double quote and extra space
    $obj_name =~ s/"//g;
    $obj_name =~ s/^\s+//;
    $obj_name =~ s/\s+$//;

    # self->{lower_case} : attribute to lower obj_name
    if ($self->{lower_case})
    {
        $obj_name = lc($obj_name);
        # then if there is non alphanumeric or the object name is a reserved word
        if ($obj_name =~ /[^a-z0-9\_\.]/ || $self->search_key_words($obj_name) || $obj_name =~ /^\d+/)
        {
            # add double quote to [schema.] object name 
            if ($obj_name !~ /^[^\.]+\.[^\.]+$/ && $obj_name !~ /^[^\.]+\.[^\.]+\.[^\.]+$/) {
                $obj_name = '"' . $obj_name . '"';
            } elsif ($obj_name =~ /^[^\.]+\.[^\.]+$/) {
                $obj_name =~ s/^([^\.]+)\.([^\.]+)$/"$1"\."$2"/;
            } else {
                $obj_name =~ s/^([^\.]+)\.([^\.]+)\.([^\.]+)$/"$1"\."$2"\."$3"/;
            }
            $obj_name = '"' . $obj_name . '"' if ($obj_name =~ /^\d+/);
        }
    }
    # add double quote to [schema.] object name 
    elsif ($obj_name !~ /^[^\.]+\.[^\.]+$/ && $obj_name !~ /^[^\.]+\.[^\.]+\.[^\.]+$/) {
        $obj_name = "\"$obj_name\"";
    } elsif ($obj_name =~ /^[^\.]+\.[^\.]+$/) {
        $obj_name =~ s/^([^\.]+)\.([^\.]+)$/"$1"\."$2"/;
    } else {
        $obj_name =~ s/^([^\.]+)\.([^\.]+)\.([^\.]+)$/"$1"\."$2"\."$3"/;
    }
    return $obj_name;
}
=head1  rewrite_end
	filter extra illegal chars behind end;
=cut

sub rewrite_end
{
	my $str = shift;

	if ($str !~ /(END\b\s*)(IF\b|LOOP\b|CASE\b|INTO\b|FROM\b|END\b|ELSE\b|AND\b|OR\b|WHEN\b|AS\b|,|\)|\(|\||[<>=]|NOT LIKE|LIKE|WHERE|GROUP|ORDER)/is) {
		$str =~ s/(END\b\s*)[\w"\.]+\s*(?:;|$)/$1;/is;
	}

	return $str;
}

1;