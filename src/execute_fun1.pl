use File::Basename;
use lib dirname(__FILE__);

use Transformation;
use RewriteUtils;
# use OracleCon;
use IO::file;
use OracleConfig;


# reading plsql from file


if($load_from_file){
    open(DATA, "<$sql_file_loc") or die "$sql_file_loc 文件无法打开, $!";
    my $plsql ;
    my @linelist = <DATA>;
    foreach my $line(@linelist){
        $plsql .= $line;
    }
    close(DATA) || die "无法关闭 $sql_file_loc 文件";
    # start converting from oracle plsql to opengauss plpgsql
    my $plpgsql ;
    {
        my $self = {};

        bless($self,"Transformation");
        print "in oracle format\n\n".$plsql."\n\n";
        print "begin converting\n";
        print "converting successfully\n\n";
        Transformation::translate($self,$plsql);
        $plpgsql = $self->{plpgsql};
        print "in opengauss format\n\n"."$plpgsql";

    }

    # write pgplsql
    my @path = split(/(.*)\.(.*)/,$sql_file_loc,2);
    my $pgsql_file_loc = $path[1] . "\(plpgsql\)\.sql";
    print "de\n".$plpgsql;
    open(DATA, ">$pgsql_file_loc") or die "$pgsql_file_loc 文件无法打开, $!";
    print DATA $plpgsql;
    close(DATA) || die "无法关闭 $pgsql_file_loc 文件";
}