package OracleCon;

use DBI;
use strict;
use OracleConfig
# usage: connect with oracle db , select procedure and return

sub get_procedures_by_user{
	my %procedures; 
	my @pro_texts;
	my $dbh = DBI->connect("dbi:Oracle:Host=$host;sid=$sid",$username,$password) or die " can not 
							connect to oracle databse!";

	# select name,line,text from all_source where owner = 'C##OPENGAUSS' and type = 'PROCEDURE'  ;
	# 之后要根据 name组合 text

	if(!$dbh){
		print "error message : connect with oracle failed ,please check your address and account."
	}
	my $sth = $dbh->prepare("select name,line,text from all_source where owner = '$username' and type = 'PROCEDURE'  ;");
	$sth->execute()or die $DBI::errstr;  
	my $temp;
	while(my @row = $sth->fetchrow()){
		my $name,$line,$text;
		$name = @row[0];
		$line = @row[1];
		$text = @row[2];
		if($temp!=$name && @pro_texts !=0){
			my $full_text;
			# connect the text
			foreach $t(@pro_texts){
				$full_text.=$t;
			}
			$full_text.="\n";
			$procedures{$name} = $full_text;
			# remove all elemets in the array 
			@pro_texts= grep( /^$/,@pro_texts);
		}
		@pro_texts[$line] = $text;
		$temp = @row[0];
	}


	$sth->finish();
	$dbh->disconnect();
	return %procedures;


	
}
sub conect{

	my ($self, $quiet) = @_;
 
	if (!defined $self->{oracle_pwd})
	{
		eval("use Term::ReadKey;") unless $self->{oracle_user} eq '/';
		$self->{oracle_user} = $self->_ask_username('Oracle') unless (defined $self->{oracle_user});
		$self->{oracle_pwd} = $self->_ask_password('Oracle') unless ($self->{oracle_user} eq '/');
	}
	my $ora_session_mode = ($self->{oracle_user} eq "/" || $self->{oracle_user} eq "sys") ? 2 : undef;

	$self->logit("ORACLE_HOME = $ENV{ORACLE_HOME}\n", 1);
	$self->logit("NLS_LANG = $ENV{NLS_LANG}\n", 1);
	$self->logit("NLS_NCHAR = $ENV{NLS_NCHAR}\n", 1);
	$self->logit("Trying to connect to database: $self->{oracle_dsn}\n", 1) if (!$quiet);

	my $dbh = DBI->connect($self->{oracle_dsn}, $self->{oracle_user}, $self->{oracle_pwd},
		{
			ora_envhp => 0,
			LongReadLen=>$self->{longreadlen},
			LongTruncOk=>$self->{longtruncok},
			AutoInactiveDestroy => 1,
			PrintError => 0,
			ora_session_mode => $ora_session_mode,
			ora_client_info => 'ora2pg '
		}
	);

	# Check for connection failure
	if (!$dbh) {
		$self->logit("FATAL: $DBI::err ... $DBI::errstr\n", 0, 1);
	}

	# Get Oracle version, needed to set date/time format
	my $sth = $dbh->prepare( "SELECT BANNER FROM v\$version" ) or return undef;
	$sth->execute or return undef;
	while ( my @row = $sth->fetchrow()) {
		$self->{db_version} = $row[0];
		last;
	}
	$sth->finish();
	chomp($self->{db_version});
	$self->{db_version} =~ s/ \- .*//;

	# Check if the connection user has the DBA privilege
	$sth = $dbh->prepare( "SELECT 1 FROM DBA_ROLE_PRIVS" );
	if (!$sth) {
		my $ret = $dbh->err;
		if ($ret == 942 && $self->{prefix} eq 'DBA') {
			$self->logit("HINT: you should activate USER_GRANTS for a connection without DBA privilege. Continuing with USER privilege.\n");
			# No DBA privilege, set use of ALL_* tables instead of DBA_* tables
			$self->{prefix} = 'ALL';
			$self->{user_grants} = 1;
		}
	} else {
		$sth->finish();
	}

	# Fix a problem when exporting type LONG and LOB
	$dbh->{'LongReadLen'} = $self->{longreadlen};
	$dbh->{'LongTruncOk'} = $self->{longtruncok};
	# Embedded object (user defined type) must be returned as an
	# array rather than an instance. This is normally the default.
	$dbh->{'ora_objects'} = 0;

	# Force datetime format
	$self->_datetime_format($dbh);
	# Force numeric format
	$self->_numeric_format($dbh);

	# Use consistent reads for concurrent dumping...
	$dbh->begin_work || $self->logit("FATAL: " . $dbh->errstr . "\n", 0, 1);
	if ($self->{debug} && !$quiet) {
		$self->logit("Isolation level: $self->{transaction}\n", 1);
	}
	$sth = $dbh->prepare($self->{transaction}) or $self->logit("FATAL: " . $dbh->errstr . "\n", 0, 1);
	$sth->execute or $self->logit("FATAL: " . $dbh->errstr . "\n", 0, 1);
	$sth->finish;

	# Force execution of initial command
	$self->_ora_initial_command($dbh);

	return $dbh;



}

sub get_procedure{
	my $self = shift;
	my $owner = shift;

	return Ora2Pg::MySQL::_get_plsql_metadata($self, $owner) if ($self->{is_mysql});

	# Retrieve all functions 
	my $str = "SELECT DISTINCT OBJECT_NAME,OWNER,OBJECT_TYPE FROM $self->{prefix}_OBJECTS WHERE (OBJECT_TYPE = 'FUNCTION' OR OBJECT_TYPE = 'PROCEDURE' OR OBJECT_TYPE = 'PACKAGE BODY')";
	$str .= " AND STATUS='VALID'" if (!$self->{export_invalid});
	if ($owner) {
		$str .= " AND OWNER = '$owner'";
		print ("Looking forward functions declaration in schema $owner.\n");
	} elsif (!$self->{schema}) {
		$str .= " AND OWNER NOT IN ('" . join("','", @{$self->{sysusers}}) . "')";
		print ("Looking forward functions declaration in all schema.\n");
	} else {
		$str .= " AND OWNER = '$self->{schema}'";
		print ("Looking forward functions declaration in schema $self->{schema}.\n");
	}
	#$str .= " ORDER BY OBJECT_NAME";
	my $sth = $self->{dbh}->prepare($str) ;
	$sth->execute or $self->logit("FATAL: " . $self->{dbh}->errstr . "\n", 0, 1);

	my %functions = ();
	my @fct_done = ();
    our @EXCLUDED_FUNCTION = ('SQUIRREL_GET_ERROR_OFFSET');
	push(@fct_done, @EXCLUDED_FUNCTION);
        while (my $row = $sth->fetch) {
                next if (grep(/^$row->[1].$row->[0]$/i, @fct_done));
                push(@fct_done, "$row->[1].$row->[0]");
		$self->{function_metadata}{$row->[1]}{'none'}{$row->[0]}{type} = $row->[2];
        }
        $sth->finish();

	# Get content of package body
	my $sql = "SELECT NAME, OWNER, TYPE, TEXT FROM $self->{prefix}_SOURCE";
	if ($owner) {
		$sql .= " WHERE OWNER = '$owner'";
	} elsif (!$self->{schema}) {
		$sql .= " WHERE OWNER NOT IN ('" . join("','", @{$self->{sysusers}}) . "')";
	} else {
		$sql .= " WHERE OWNER = '$self->{schema}'";
	}
	$sql .= " AND TYPE <> 'PACKAGE'";
	$sql .= " ORDER BY OWNER, NAME, LINE";
	$sth = $self->{dbh}->prepare($sql) or $self->logit("FATAL: " . $self->{dbh}->errstr . "\n", 0, 1);
    
}
1;