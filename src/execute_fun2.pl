use File::Basename;
use lib dirname(__FILE__);

use Transformation;
use RewriteUtils;
use OracleCon;
use IO::file;
use OracleConfig;


# reading plsql from oracle


if(!$load_from_file){

  # oracle connecting 
  if($host != ""&&$sid!="" && $username != "" && $password != "" && $convert_location!=""){
    my %procedures = OracleCon::get_procedures_by_user();
    my $index=1;
    while(($name, $plsql) = each(%procedures)){
      print "converting procedure \( index : $index\,procedure name : $name)\n";
      # start converting from oracle plsql to opengauss plpgsql
      my $plpgsql ;
      my $self = {};

      bless($self,"Transformation");
      print "in oracle format\n\n".$plsql."\n\n";
      print "begin converting\n";
      print "converting successfully\n\n";
      Transformation::translate($self,$plsql);
      $plpgsql = $self->{plpgsql};
      print "in opengauss format\n\n"."$plpgsql";

      # write pgplsql
      my $pgsql_file_loc = $convert_location . "$name\(plpgsql\)\.sql";

      open(DATA, ">$pgsql_file_loc") or die "$pgsql_file_loc 文件无法打开, $!";
      print DATA $plpgsql;
      close(DATA) || die "无法关闭 $pgsql_file_loc 文件";

    }

    open(DATA, "<$sql_file_loc") or die "$sql_file_loc 文件无法打开, $!";
    my $plsql ;
    my @linelist = <DATA>;
    foreach my $line(@linelist){
        $plsql .= $line;
    }

  }else  {
    print "error !, before executing ,check that oracle information is complete ";
  }
}