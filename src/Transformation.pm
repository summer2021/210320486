package Transformation;


use RewriteUtils;
# use OracleCon;
# use DBI;
use IO::File;
use Benchmark;
use Encode;
use IO::Handle;
use IO::Pipe;
use File::Basename;

use strict;

# convert oracle types to opengauss types
our %TYPE = (
    # char nchar varchar varchar2 nvarchar2 is also available in opengauss
	'NVARCHAR' => 'varchar',
	'STRING' => 'varchar',
	'VARCHAR2' => 'text',
    # oracle only support number, programe will convert number to numeric 
    # such as number(5,2) -> numeric(5,2) (which is available in opengauss)
    # integer is available
    'NUMBER' => 'numeric',
    'INT ' => 'numeric',
    'BINARY_INTEGER' => 'integer',
    'PLS_INTEGER' => 'integer',
    # long is up to 2GB ,varchar2 is up to 10MB, so text is meet your needs 
	# long raw is not support in opengauss, so you can use bytea instead.
	'LONG' => 'text',
	'LONG RAW' => 'bytea',
    # The date data type is used to store the date and time
	# information.  timestamp should match all needs.
	'DATE' => 'timestamp',
    # clob is same as text,nclob,befile is not support in opengauss
	'NCLOB' => 'text',
    # column storage doesn't support blob , so change it to bytea
	'BLOB' => 'bytea', 
	'BFILE' => 'bytea', 
	'ROWID' => 'oid',
	'UROWID' => 'oid',
    # float contains float4 and float8 double precision
    # dec is available'
    # BINARY_DOUBLE is available, BINARY_FLOAT is not 
	'FLOAT' => 'double precision',
    'BINARY_FLOAT' => 'double precision',
	'XMLTYPE' => 'xml',
	'SDO_GEOMETRY' => 'geometry',
);


# keywords both in opengauss and oracle
our @Opgs_Key_Words = qw(
	ALL ANALYSE ANALYZE AND ANY ARRAY AS ASC ASYMMETRIC AUTHORIZATION BINARY
	BOTH CASE CAST CHECK COLLATE COLLATION COLUMN CONCURRENTLY CONSTRAINT CREATE
	CROSS CURRENT_CATALOG CURRENT_DATE CURRENT_ROLE CURRENT_SCHEMA CURRENT_TIME
	CURRENT_TIMESTAMP CURRENT_USER DEFAULT DEFERRABLE DESC DISTINCT DO ELSE END
	EXCEPT FALSE FETCH FOR FOREIGN FREEZE FROM FULL GRANT GROUP HAVING ILIKE IN
	INITIALLY INNER INTERSECT INTO IS ISNULL JOIN LATERAL LEADING LEFT LIKE LIMIT
	LOCALTIME LOCALTIMESTAMP NATURAL NOT NOTNULL NULL OFFSET ON ONLY OR ORDER OUTER
	OVERLAPS PLACING PRIMARY REFERENCES RETURNING RIGHT SELECT SESSION_USER SIMILAR
	SOME SYMMETRIC TABLE TABLESAMPLE THEN TO TRAILING TRUE UNION UNIQUE USER USING
	VARIADIC VERBOSE WHEN WHERE WINDOW WITH
);

our %Exception_Convert = (
	'INVALID_CURSOR' => 'invalid_cursor_state',
	'ZERO_DIVIDE' => 'division_by_zero',
	'STORAGE_ERROR' => 'out_of_memory',
	'INTEGRITY_ERROR' => 'integrity_constraint_violation',
	'VALUE_ERROR' => 'data_exception',
	'INVALID_NUMBER' => 'data_exception',
	'INVALID_CURSOR' => 'invalid_cursor_state',
	'NO_DATA_FOUND' => 'no_data_found',
	'LOGIN_DENIED' => 'connection_exception',
	'TOO_MANY_ROWS'=> 'too_many_rows',
	# 'PROGRAM_ERROR' => 'INTERNAL ERROR',
	# 'ROWTYPE_MISMATCH' => 'DATATYPE MISMATCH'
);

# keywords which is not allowed in opengauss
our @Oracle_Unique_FUNCTION = qw(
	APPENDCHILDXML ASSCIISTR BFILENAME CHARTOROWID CLUSTER_ID CLUSTER_PROBABILITY
	CLUSTER_SET CUBE_TABLE  DATAOBJ_TO_PARTITION DELETEXML DEPTH DEFER DUMP EXISTNODE
	FROM_TZ GROUP_ID INSERTCHILDXML ITERATION_NUMBER LNNVL MAKE_REF NEW_TIME
	NLS_CHARSET_DECL_LEN  NLS_CHARSET_ID NLS_CHARSET_NAME NLS_INITCAP
	ORA_DST_AFFECTED ORA_DST_CONVERT ORA_DST_ERROR 
	PERCENTILE_CONT PERCENTILE_DISC
	POWERMULTISET POWERMULTISET_BY_CARDINALITY
	PREDICTION PREDICTION_BOUNDS PREDICTION_COST PREDICTION_DETAILS PREDICTION_PROBABILITY PREDICTION_SET
	PRESENTNNV RATIO_TO_REPORT 
	REF REFTOHEX REMAINDER 
	ROWIDTOCHAR ROWIDTONCHAR 
	SESSIONTIMEZONE SOUNDE
	STATS_BINOMIAL_TEST STATS_CROSSTAB STATS_F_TEST STATS_KS_TEST STATS_MODE STATS_MODE STATS_ONE_WAY_ANOVA STATS_T_TEST_* STATS_WSR_TEST
	SYS_CONNECT_BY_PATH SYS_CONTEXT SYS_DBURIGEN SYS_EXTRACT_UTC SYS_GUID SYS_TYPEID SYS_XMLAGG SYS_XMLGEN
	TO_LOB TO_BCLOB TO_MULTI_BYTE TO_SINGLE_BYTE 
	XMLAGG XMLCAST XMLCDATA XMLCOLATTVAL XMLCOMMENT XMLDIFF XMLEXISTS XMLFOREST XMLISVALID XMLPARSE XMLPATCH XMLPI
	XMLQUERY XMLROOT XMLSEQUENCE XMLSERIALIZE XMLTABLE XMLTRANSFORM

);




=head1   new entity of tansformation class (entry program)

Create transformation entity and translate the input plsql 
    input: oracle procedure text
    output: prompt message and procedure in the form of opengauss 

=cut 

sub translate{
	my ($self,$plsql) = @_;
    # 1 means load from procedure file ,0 means load from database 
    $self->{load_from_file} = 1;
    # create new  entity
    $self->init($plsql);
    return $self;
}


=head1  init function

Create transformation entity
    input: oracle procedure text
    output: prompt message and procedure in the form of opengauss 

=cut

sub init(){
	my ($self,$plsql) = @_;
    
    # load from oracle db (to be done)
    # if(!$self->{load_from_file}){
    #     $Connection = OracleCon->Connect();
    #     $plsql = $Connection->get_procedure();
    # }

    #remove annotation
    $plsql = $self->remove_annotation($plsql);
	my %result ={};
	%result = $self->break_up_plsql($plsql);
	# $function =~ s/\r//gs;
	# my @lines = split(/\n/, $function);
	# map { s/^\/$//; } @lines;
	# return join("\n", @lines);
	# return plpgsql for opengauss after conbination
	$self->{plpgsql} = $result{begin}."\t" . $result{procedure_name}. $result{args} ."\n". $result{as_or_is}
            ."\n" . $result{define} . $result{body} . $result{end};
}

=head1  break_up_plsql

split input text into serveral parts and detail information
    fisrt split plsql into declare/body/exception/end parts;
=cut

sub break_up_plsql{
    my ($self,$plsql) = @_;
    return if(!$plsql);

    # remove annotation 
    $plsql = $self->remove_annotation($plsql);
    my $procedure_temp;
    my %result;
    my $remain;
    ($result{declare},$remain) = split(/\bBEGIN\b/i,$plsql,2);
    return if (!$remain);
    if( $result{declare} =~ /(.*?\bPROCEDURE\s+)([^\s\(]+)\s*/is ){
        # $result{begin} : create or replace procedure
        $result{begin} = $1;
		$result{procedure_name} = $2;
		if($result{begin} =~ /^[^\w]*PROCEDURE[^\w]*$/igs){
			$result{begin} = "CREATE OR REPLACE PROCEDURE";
		}


        $procedure_temp = $result{procedure_name};
        $result{procedure_name} =~ s/["']//g;

        if($result{declare} =~ /(.*?)\b(PROCEDURE)\s+([^\s\(]+)\s*(\([^\)]*\))/is){
            $result{args} = $4;
            # checking args naming 
        }elsif( $result{declare} =~ /(.*?)\b(PROCEDURE)\s+([\s\t\n]*)\s+(RETURN|IS|AS)/is){
            $result{args} = "\(\)";
        }
        if($result{declare} =~ /\b(IS|AS)\b(.*?)$/is){
            $result{as_or_is} = $1." \$\$";
            # $result{define} declare variables such   as a_pos1 INTEGER;a_pos2 INTEGER;
            $result{define} = $2;
			$result{define} =~ s/^\n*//igs;
        }

    }else{
        return;
    }
    if($result{procedure_name}){
        if($result{procedure_name} =~ /^([^\.]+)\.([^\.\s]+)/is){
            $result{schema_name} = $1;
        }
        $result{procedure_name} = $self->rewrite_object_name($result{procedure_name});
    }
    # add define if for ** in ** loop ( add record)
    my $tmp_code = $remain;
    while ($tmp_code =~ s/\bFOR\s+([^\s]+)\s+IN(.*?)LOOP//is)
    {
        my $varname = quotemeta($1);
        my $clause = $2;
        if ($result{define} !~ /\b$varname\s+/is) {
            chomp($result{define});
            # When the cursor is refereing to a statement, declare
            # it as record otherwise it don't need to be replaced
            if ($clause =~ /\bSELECT\b/is) {
                $result{define} .= "\n  $varname RECORD;\n";
            }
        }
    }

    # convert types to opengauss format
    $result{define} = Transformation::convert_types($result{define});

    # convert cursor define 
    $result{define} = Transformation::convert_cursor($result{define});

	# add declare describtion 
	if($result{define}!~ /^[\s\n\t]$/is && 
		($result{define}!~ /^[\n\s\t]*\bDECLARE/is && $result{define}!~ /^[\n\s\t]*\bSET/is ) ){
			$result{define}= "DECLARE \n".$result{define};
	}
    if ($result{define} && $result{define} !~ /;\s*$/s){
        $result{define}.= ';';
    }


    # ############ define rewrite end ################


    # rewrite args     input args :  such as (v1 in int,v2 out date);
    $result{args} =~ s/\s+/ /igs;
    # if args != null  change args types 
    if($result{args} !~ /\(\)/igs){
       $result{args} = Transformation::convert_args($result{args});
    }
	

    # ############ args rewrite end ##############
    # ############ declare rewrite end ############

	# reweite plsql body 
	# replace some unsupported function in opengauss, such as string_add and so on.
	$result{body} = convert_body_plsql($remain);
	$result{body} = "\nBEGIN\n".$result{body};
    # end part
    my @split_end = split(/(.*)END(.*)$/igs,$result{body} ,2);
    my $end = $split_end[2];
	$result{body} = $split_end[1];
	$end = "END".$end;
	if($result{body} !~ /\n[\s\t]+$/igs){
		$end = "\n".$end;
	}

    if($end =~ /\;[\s\n\t]*$/g){
        if($end =~ /\s*\/\s*/ ){
            $end ="\$\$ LANGUAGE plpgsql;";
        }elsif ($end =~ /\n[\s\n\t]*$/g){
            $end.="\$\$ LANGUAGE plpgsql;";
        }else{
			$end.="\$\$ LANGUAGE plpgsql;";
		}
    }

	$result{end} = $end;
	return %result;

} 


=head1  convert_body_plsql
    the promgrame split input plsql into two parts : declare/body
    declare is from create to begin 
    body is from begin to end;
=cut

sub convert_body_plsql{
    my $plsql_body = shift;
    return if ($plsql_body =~ /^[\s\t\n]*$/);

	################### below is some rules to rewrite ############
	# for example : rewrite variable name which is illegeal in opengauss 
	# remove or replace some functions that are unsupport in opengauss (string_agg....)  etc.

	# first remove some extra space between operators
	$plsql_body =~ s/=\s+>/=>/gs;
	$plsql_body =~ s/<\s+=/<=/gs;
	$plsql_body =~ s/>\s+=/>=/gs;
	$plsql_body =~ s/!\s+=/!=/gs;
	$plsql_body =~ s/<\s+>/<>/gs;
	$plsql_body =~ s/:\s+=/:=/gs;
	$plsql_body =~ s/\|\s+\|/\|\|/gs;
	$plsql_body =~ s/!=([+\-])/!= $1/gs;

    # remove sys 
	$plsql_body =~ s/\bSYS\.//igs;
	# replace systimestamp 
	$plsql_body =~ s/\bSYSTIMESTAMP\b/ pg_systimestamp/igs;

	# replace EXEC function into variable, ex: EXEC :a := test(:r,1,2,3);
	$plsql_body =~ s/\bEXEC\s+:([^\s:]+)\s*:=/SELECT INTO $2/igs;

	# replace simple EXEC function call by SELECT function
	$plsql_body =~ s/\bEXEC(\s+)/SELECT$1/igs;

	# INSERTING|DELETING|UPDATING -> TG_OP = 'INSERT'|'DELETE'|'UPDATE'
	$plsql_body =~ s/\bINSERTING\b/TG_OP = 'INSERT'/igs;
	$plsql_body =~ s/\bDELETING\b/TG_OP = 'DELETE'/igs;
	$plsql_body =~ s/\bUPDATING\b/TG_OP = 'UPDATE'/igs;

	# replace on commit ** definition
	$plsql_body =~ s/ON\s+COMMIT\s+PRESERVE\s+DEFINITION/ON COMMIT PRESERVE ROWS/igs;
	$plsql_body =~ s/ON\s+COMMIT\s+DROP\s+DEFINITION/ON COMMIT DROP/igs;

	my $conv_current_time = 'clock_timestamp()';
	# Replace sysdate +/- N by localtimestamp - 1 day intervel
	$plsql_body =~ s/\bSYSDATE\s*(\+|\-)\s*(\d+)/$conv_current_time $1 interval '$2 days'/igs;
    # convert array   arr(i) to arr[i]
    $plsql_body =~ s/\b([a-z0-9_]+)\(([^\(\)]+)\)(\.[a-z0-9_]+)/$1\[$2\]$3/igs;

	#replace DATIMEZONE with  PG_TIMEZONE_NAMES 
	$plsql_body =~ s/DATIMEZONE/PG_TIMEZONE_NAMES/igs;


	# remove/replace some function
	# nextval is different from oracle's to opengauss's   
    # oracle just like ***.nextval  opengauss : nextval('***'). 
    $plsql_body =~ s/\s*\b(\w+)\.nextval/nextval('\L$1\E')/isg;
    $plsql_body =~ s/\s*\b(\w+)\.currval/currval('\L$1\E')/isg;
	# Try to fix call to string_agg with a single argument (allowed in oracle)
	$plsql_body =~ s/\bstring_agg\(([^,\(\)]+)\s+(ORDER\s+BY)/string_agg($1, '' $2/igs;
	# remove to_clob();
	$plsql_body =~ s/TO_CLOB\s*\(/\(/igs;
	# Replace call to trim into btrim
	$plsql_body =~ s/\bTRIM\s*\(([^\(\)]+)\)/trim(both $1)/igs;
	# replace XMLTYPE function
	$plsql_body =~ s/XMLTYPE\s*\(\s*([^,]+)\s*,[^\)]+\s*\)/xmlparse(DOCUMENT, convert_from($1, 'utf-8'))/igs;
	$plsql_body =~ s/XMLTYPE\.CREATEXML\s*\(\s*[^\)]+\s*\)/xmlparse(DOCUMENT, convert_from($1, 'utf-8'))/igs;
	# rewrite replace(a,b) with three argument
	$plsql_body =~ s/REPLACE\s*\((.*?),(.*?)\)/replace($1, $2, '')/is;

	# # replace DEFAULT empty_blob() and empty_clob()
	# my $empty = 'NULL' ;
	# $plsql_body =~ s/(empty_blob|empty_clob)\s*\(\s*\)/$empty/is;
	# $plsql_body =~ s/(empty_blob|empty_clob)\b/$empty/is;

	# rewrite nvl2
	# nvl is supported in opengauss but nvl2 isn't, so replace nvl2 by decode
	$plsql_body =~ s/NVL2\s*\((.*?),(.*?),(.*?)\)/DECODE \($1,NULL,$3,$2\)/isg;


	# comment DBMS_OUTPUT.ENABLE calls
	$plsql_body =~ s/(DBMS_OUTPUT.ENABLE[^;]+;)/-- $1/isg;
	# DBMS_LOB.GETLENGTH can be replaced by binary length.
	$plsql_body =~ s/DBMS_LOB.GETLENGTH/octet_length/igs;
	# DBMS_LOB.SUBSTR can be replaced by SUBSTR()
	$plsql_body =~ s/DBMS_LOB.SUBSTR/substr/igs;
	
	# replace length by char_length
	$plsql_body =~ s/LENGTH/CHAR_LENGTH/igs;
	
	# replace nchr by chr 
	$plsql_body =~ s/NCHR/CHR/igs;

	#replace NLS_UPPER/NLS_LOWER with upper/lower
	$plsql_body =~ s/NLS_UPPER/UPPER/igs;
	$plsql_body =~ s/NLS_LOWER/lower/igs;

	#replace months_between()  by timestampdiff(month)
	$plsql_body =~ s/MONTHS_BETWEEN\(([^\(\)\,]+)\,([^\(\),]+)\)/TIMESTAMPDIFF(MONTH,$1,$2)/igs;

	#replace ora_hash by hll_hash_any; also you can replace it by hill_hash_XXX,XXX represent special types
	$plsql_body =~ s/ORA_HASH/HLL_HASH_ANY/igs;

	# perceent_rank/cume_dist in opengauss doesn't accept any arguement
	$plsql_body =~ s/PERCENT_RANK\(.*\)/PERCENT_RANK\(\)/igs;
	$plsql_body =~ s/CUME_DIST\(.*\)/CUME_DIST\(\)/igs;

	# replace special IEEE 754 values for not a number and infinity
	$plsql_body =~ s/BINARY_(FLOAT|DOUBLE)_NAN/'NaN'/igs;
	$plsql_body =~ s/([\-]*)BINARY_(FLOAT|DOUBLE)_INFINITY/'$1Infinity'/igs;
	$plsql_body =~ s/'([\-]*)Inf'/'$1Infinity'/igs;


	# replace to_nchar by ranslate 
	$plsql_body =~ s/TO_NCHAR/RANSLATE/igs; 

	# replace to_binary_double  to_binary_float  to_blob by cast
	$plsql_body =~ s/TO_BINARY_DOUBLE\((.*)\)/CAST\($1,BINARY_DOUBLE\)/igs;
	$plsql_body =~ s/TO_BINARY_FLOAT\((.*)\)/CAST\($1,BINARY_FLOAT\)/igs;
	$plsql_body =~ s/TO_BLOB\((.*)\)/CAST\($1,BLOB\)/igs;

	# replace TO_TIMESTAMP_TZ by to_data ; return type timestamp with zone
	$plsql_body =~ s/TO_TIMESTAMP_TZ\((.*)\,(.*)\)/TO_DATA\($1\,$2\)/igs; 

	# replace PIPE ROW by RETURN NEXT
	$plsql_body =~ s/PIPE\s+ROW\s*/RETURN NEXT /igs;
	$plsql_body =~ s/(RETURN NEXT )\(([^\)]+)\)/$1$2/igs;

    # replace the UTC convertion with the PG syntaxe
	$plsql_body =~ s/SYS_EXTRACT_UTC\s*\(([^\)]+)\)/($1 AT TIME ZONE 'UTC')/is;
	# remove call to XMLCDATA, there's no such function with PostgreSQL
	$plsql_body =~ s/XMLCDATA\s*\(([^\)]+)\)/'<![CDATA[' || $1 || ']]>'/is;
	# remove call to getClobVal() or getStringVal, no need of that
	$plsql_body =~ s/\.(GETCLOBVAL|GETSTRINGVAL|GETNUMBERVAL|GETBLOBVAL)\s*\(\s*\)//is;
	# add the name keyword to XMLELEMENT
	$plsql_body =~ s/XMLELEMENT\s*\(\s*/XMLELEMENT(name /is;


	# dup_val_on_index => unique_violation : already exist exception
	$plsql_body =~ s/\bdup_val_on_index\b/unique_violation/igs;

	# replace exception   
    # reaise_application_error -> raise exception 
    # two ways to show exception   here choose the one that doesn't contain error code

    $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+),\s*(true|false)\s*\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . ";"/iges;
	$plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+)\)\s*;/"RAISE EXCEPTION " . remove_named_parameters($2) . ";"/iges;
	$plsql_body =~ s/DBMS_STANDARD\.RAISE EXCEPTION/RAISE EXCEPTION/igs;

    # for the reason that i doesn't find  the error code in opengauss (maybe) 
	# $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+),\s*(true|false)\s*\)\s*;/"RAISE EXCEPTION '%', " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	# $plsql_body =~ s/\braise_application_error\s*\(\s*([^,]+)\s*,\s*([^;]+)\)\s*;/"RAISE EXCEPTION '%', " . remove_named_parameters($2) . " USING ERRCODE = " . set_error_code(remove_named_parameters($1)) . ";"/iges;
	# $plsql_body =~ s/DBMS_STANDARD\.RAISE EXCEPTION/RAISE EXCEPTION/igs;


	# rewrite having group by
	$plsql_body =~ s/\bHAVING\b((?:(?!SELECT|INSERT|UPDATE|DELETE|WHERE|FROM).)*?)\bGROUP BY\b((?:(?!SELECT|INSERT|UPDATE|DELETE|WHERE|FROM).)*?)((?=UNION|ORDER BY|LIMIT|INTO |FOR UPDATE|PROCEDURE|\)\s+(?:AS)*[a-z0-9_]+\s+)|$)/GROUP BY$2 HAVING$1/gis;

	# add strict 
	$plsql_body =~ s/\b(SELECT\b[^;]*?INTO)(.*?)(EXCEPTION.*?(?:NO_DATA_FOUND|TOO_MANY_ROW))/$1 STRICT $2 $3/igs;
	$plsql_body =~ s/\b((?:SELECT|EXECUTE)\s+[^;]*?\s+INTO)(\s+(?!STRICT))/$1 STRICT$2/igs;
	$plsql_body =~ s/(INSERT\s+INTO\s+)STRICT\s+/$1/igs;

	# remove some extra chars at the end of keywords
	$plsql_body =~ s/\b(END\s*[^;\s]+\s*(?:;|$))/rewrite_end($1)/iges;

	# rewrite comment in CASE between WHEN and THEN
	$plsql_body =~ s/(\s*)(WHEN\s+[^\s]+\s*)(\%ORA2PG_COMMENT\d+\%)(\s*THEN)/$1$3$1$2$4/igs;

	# sqlcode -> sqlstate
	$plsql_body =~ s/\bSQLCODE\b/SQLSTATE/igs;
	# remove MSSYS.
	$plsql_body =~ s/\bMDSYS\.//igs;
	# rewrite reverse
	$plsql_body =~ s/\bFOR(.*?)IN\s+REVERSE\s+([^\.\s]+)\s*\.\.\s*([^\s]+)/FOR$1IN REVERSE $3..$2/isg;

	# replace exit at end of cursor
	$plsql_body =~ s/EXIT\s+WHEN\s+([^\%;]+)\%\s*NOTFOUND\s*;/EXIT WHEN NOT FOUND; \/\* apply on $1 \*\//isg;
	$plsql_body =~ s/EXIT\s+WHEN\s+\(\s*([^\%;]+)\%\s*NOTFOUND\s*\)\s*;/EXIT WHEN NOT FOUND;  \/\* apply on $1 \*\//isg;

	$plsql_body =~ s/EXIT\s+WHEN\s+([^\%;]+)\%\s*NOTFOUND\s+([^;]+);/EXIT WHEN NOT FOUND $2;  \/\* apply on $1 \*\//isg;
	$plsql_body =~ s/EXIT\s+WHEN\s+\(\s*([^\%;]+)\%\s*NOTFOUND\s+([^\)]+)\)\s*;/EXIT WHEN NOT FOUND $2;  \/\* apply on $1 \*\//isg;
	
	# replace UTL_MATH function by fuzzymatch function
	$plsql_body =~ s/UTL_MATCH.EDIT_DISTANCE/levenshtein/igs;

    # replace UTL_ROW.CAST_TO_RAW function by encode function
    $plsql_body =~ s/UTL_RAW.CAST_TO_RAW\s*\(\s*([^\)]+)\s*\)/encode($1::bytea, 'hex')::bytea/igs;

	# replace illegal outerjoin by (+)
	$plsql_body =~ s/\%OUTERJOIN\d+\%/\(\+\)/igs;

	# replace <> null -> is not null 
	$plsql_body =~ s/\s*(<>|\!=)\s*NULL/ IS NOT NULL/igs;
	
	# $plsql_body =~ s/(?!:)(.)=\s*NULL/$1 IS NULL/igs;

	# add from 
	$plsql_body =~ s/(\bDELETE\s+)(?!FROM|WHERE|RESTRICT|CASCADE|NO ACTION)\b/$1FROM /igs;


	# Oracle doesn't require parenthesis after VALUES, PostgreSQL has
	# similar proprietary syntax but parenthesis are mandatory
	$plsql_body =~ s/(INSERT\s+INTO\s+(?:.*?)\s+VALUES\s+)([^\(\)\s]+)\s*;/$1\($2.*\);/igs;

	# replace some windows function issues with KEEP (DENSE_RANK FIRST ORDER BY ...)
	$plsql_body =~ s/\b(MIN|MAX|SUM|AVG|COUNT|VARIANCE|STDDEV)\s*\(([^\)]+)\)\s+KEEP\s*\(DENSE_RANK\s+(FIRST|LAST)\s+(ORDER\s+BY\s+[^\)]+)\)\s*(OVER\s*\(PARTITION\s+BY\s+[^\)]+)\)/$3_VALUE($2) $5 $4)/igs;

	$plsql_body =~ s/TIMESTAMP\s*('[^']+')/$1/igs;

	my $tmp_code = $plsql_body;
	while ($tmp_code =~ s/\bFOR\s+([^\s]+)\s+IN(.*?)LOOP//is)
	{
		my $varname = $1;
		my $clause = $2;
		my @code = split(/\bBEGIN\b/i, $plsql_body);
		if ($code[0] !~ /\bDECLARE\s+.*\b$varname\s+/is)
		{
			# When the cursor is refereing to a statement, declare
			# it as record otherwise it don't need to be replaced
			if ($clause =~ /\bSELECT\b/is)
			{
				# append variable declaration to declare section
				if ($plsql_body !~ s/\bDECLARE\b/DECLARE\n  $varname RECORD;/is)
				{
					# No declare section
					$plsql_body = "DECLARE\n  $varname RECORD;\n" . $plsql_body;
				}
			}
		}
	}

	# oracle and opengauss both has the path function , but it's totally different in the usage

	# if( $plsql_body=~ /PATH/is){

	# }

	# previous :the same as path 
	# if( $plsql_body=~ /PREVIOUS/is){

	# }

	# convert cursor declaration
	$plsql_body = Transformation::convert_cursor($plsql_body);

	# replace error code by opengauss error code
	foreach my $e (keys %Exception_Convert) {
		$plsql_body =~ s/\b$e\b/$Exception_Convert{"\U$e\L"}/igs;
	}



    return $plsql_body;

}


=head1  rewrite_end
	filter extra illegal chars behind end;
=cut

sub rewrite_end
{
	my $str = shift;

	if ($str !~ /(END\b\s*)(IF\b|LOOP\b|CASE\b|INTO\b|FROM\b|END\b|ELSE\b|AND\b|OR\b|WHEN\b|AS\b|,|\)|\(|\||[<>=]|NOT LIKE|LIKE|WHERE|GROUP|ORDER)/is) {
		$str =~ s/(END\b\s*)[\w"\.]+\s*(?:;|$)/$1;/is;
	}

	return $str;
}


=head1  search_key_words

search key words in plsql 
    @Returns:
        1 : keywords both in oracle and opengauss
        2 : keywords only in oracle 
        0 : no keywords
=cut

sub search_key_words{
    my ($self,$words) = @_;
    if( $words && grep(/^\Q$words\E$/i,@Opgs_Key_Words) ){
        return 1;
    }
    if( $words && grep(/^\Q$words\E$/i,@Oracle_Unique_FUNCTION) ){
        return 2;
    }
    return 0;

}


=head1  remove_annotation

remove the annotations in the plsql 
    usually start wit /* */

=cut

# ------- change later ----------  add annotation mark later 
sub remove_annotation{
    my ($self,$plsql) = @_;
    while($plsql=~ s/\/\*(.*?)\*\///is){

    };
    my @lines = split(/\n/,$plsql);
    $plsql = '';
    for(my $i=0;$i<=$#lines;++$i){
		# remove annotation like  --
		#  v_host OUT VARCHAR2,  -- This will be passed back
        $lines[$i] =~ s/\s*\-\-.*$//is;
        if(!$lines[$i] =~ /^\s*$/){
            $plsql .= $lines[$i]."\n";
        }
    }
    return $plsql;

}


=head1  convert_args

    useages: convert args in plsql .
        change in out ,type ....

=cut

sub convert_args{

    my $args=shift;
    return if(!$args);
    # alter syntax 
    # remove nocopy
    $args =~ s/\s*NOCOPY//igs;
    # remove %rowtype
    #$args =~ s/\%ROWTYPE//igs;
    # empty blob() -> default null
    $args =~ s/:=/DEFAULT/igs;
    $args =~ s/\bINOUT\b/IN OUT/igs;
    $args =~ s/\s+DEFAULT\s+EMPTY_[CB]LOB\(\)/DEFAULT NULL/igs;
    # change types

    $args = Transformation::convert_types($args);

    # set default inorder (if a has default value ,b behind a must has default value null
    # so set default value null
    # first remove (  ) in order to add default value
    $args =~ s/^(.*?)\((.*?)\)(.*?)/$1$2$3/is;
    my $default_index = 0;
    my @args_inorder = split(',', $args);
    for (my $i = 0; $i <= $#args_inorder; $i++)
    {
        $default_index = 1 if ($args_inorder[$i] =~ /\s+DEFAULT\s/i);
        if ($default_index && $args_inorder[$i] !~ /\s+DEFAULT\s/i)
        {
            # add default value null if it is not out 
            if ( $args_inorder[$i] !~ /[,\(\s]OUT[\s,\)]/i && $args_inorder[$i] !~ /^OUT\s/i) {
                $args_inorder[$i] .= ' DEFAULT NULL';
            }
        }
    }
    # add ()
    $args =  '(' . join(',', @args_inorder) . ')' ;

    return $args;
}


=head1  convert_types

    useages: convert types in plsql .
        
=cut

sub convert_types{
    my $str=shift;
	my %data_type = %TYPE;
	# remove sys. schema type
	$str =~ s/\bSYS\.//igs;
	$str =~ s/with local time zone/with time zone/igs;
	# remove precision for RAW|BLOB as type modifier is not allowed for type "bytea"
	$str =~ s/\b(RAW|BLOB)\s*\(\s*\d+\s*\)/$1/igs;

    # convert types in regex form such  as  number|time|int|blob|clob
	my @types = keys %data_type;
	map { s/\(/\\\(/; s/\)/\\\)/; } @types;
	my $types_regex = join('|', @types);

    # search type regex   in the form of numeric()
    # data( , )
	while ($str =~ /(.*)\b($types_regex)(\s*\(([^\)]+)\))/i){
		my $backstr = $1;
		my $type = uc($2);
		my $args = $3;

		# remove extra CHAR or BYTE information from column type
		$args =~ s/\s*(CHAR|BYTE)\s*$//i;
		if ($backstr =~ /_$/)
		{
		    $str =~ s/\b($types_regex)\s*\(([^\)]+)\)/$1\%\|$2\%\|\%/is;
		    next;
		}
		my ($precision, $scale) = split(/\s*,\s*/, $args);
        #  NUMBER(*,10) or NUMBER(*)
		$precision = 38 if ($precision eq '*'); 
		my $len = $precision || 0;
		$scale ||= 0;
		$len =~ s/\D//;
		if ( $type =~ /CHAR|STRING/i )
		{
			# Type CHAR have default length set to 1
			# Type VARCHAR(2) must have a specified length
			$len = 1 if (!$len && (($type eq "CHAR") || ($type eq "NCHAR")));
			# $str =~ s/\b$type\b\s*\([^\)]+\)/$data_type{uc($type)}\%\|$len\%\|\%/is;
		}
		elsif ($type =~ /TIMESTAMP/i)
		{
			$len = 6 if ($len > 6);
			# $str =~ s/\b$type\b\s*\([^\)]+\)/timestamp\%\|$len%\|\%/is;
 		}
		elsif ($type =~ /INTERVAL/i)
		{
 			# Interval precision for year/month/day is not supported by PostgreSQL
 			$str =~ s/(INTERVAL\s+YEAR)\s*\(\d+\)/$1/is;
 			$str =~ s/(INTERVAL\s+YEAR\s+TO\s+MONTH)\s*\(\d+\)/$1/is;
 			$str =~ s/(INTERVAL\s+DAY)\s*\(\d+\)/$1/is;
			# maximum precision allowed for seconds is 6
			if ($str =~ /INTERVAL\s+DAY\s+TO\s+SECOND\s*\((\d+)\)/)
			{
				if ($1 > 6) {
					$str =~ s/(INTERVAL\s+DAY\s+TO\s+SECOND)\s*\(\d+\)/$1(6)/i;
				}
			}
		}		
        # change number precision
        elsif ($type eq "NUMBER")
		{   if (!$scale)
			{
				if ($precision)
				{
                    if ($precision < 5) {
                        $str =~ s/\b$type\b\s*\([^\)]+\)/smallint/is;
                    } elsif ($precision <= 9) {
                        $str =~ s/\b$type\b\s*\([^\)]+\)/integer/is;
                    } elsif ($precision <= 19) {
                        $str =~ s/\b$type\b\s*\([^\)]+\)/bigint/is;
                    } else {
                        $str =~ s/\b$type\b\s*\([^\)]+\)/numeric($precision)/is;
                    }
				}
			}
			else
			{
                if ($precision eq '') {
                    $str =~ s/\b$type\b\s*\([^\)]+\)/decimal(38, $scale)/is;
                } elsif ($precision <= 6) {
                    $str =~ s/\b$type\b\s*\([^\)]+\)/real/is;
                } else {
                    $str =~ s/\b$type\b\s*\([^\)]+\)/double precision/is;
                }
			}
		}
        $str =~ s/(.*)\b($types_regex)(\s*\(([^\)]+)\))/$1$data_type{uc($2)}$3/i
    }
    while ($str =~ s/(.*)\b($types_regex)\s*/$1$data_type{uc($2)}/is){
	};


    # cut off black;
	$str =~ s/;[ ]+/;/gs;
	return $str;

}

=head1  convert_cursor

    useages: convert args in plsql .
        change in out ,type ....

=cut

sub convert_cursor
{
	my $str = shift;
	# remove cursor (i in int)  in is default
	while ($str =~ s/(\bCURSOR\b[^\(]+)\(([^\)]+\bIN\b[^\)]+)\)/$1\(\%\%CURSORREPLACE\%\%\)/is) {
		my $args = $2;
		$args =~ s/\bIN\b//igs;
		$str =~ s/\%\%CURSORREPLACE\%\%/$args/is;
	}

	# replace %rowtype
	$str =~ s/\bTYPE\s+([^\s]+)\s+(IS\s+REF\s+CURSOR|REFCURSOR)\s+RETURN\s+[^\s\%]+\%ROWTYPE;/$1 REF CURSOR;/isg;

	# replace sys_refcursor
	$str =~ s/\bSYS_REFCURSOR\b/REF CURSOR/isg;

	# replace open 
	$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)((?:[^;]+?)USING)/$1 EXECUTE$2/isg;
	#$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)\s+((?!EXECUTE)(?:[^;]+?)\|\|)/$1 EXECUTE $2/isg;
	$str =~ s/(OPEN\s+(?:[^;]+?)\s+FOR)\s+([^\s]+\s*;)/$1 EXECUTE $2/isg;
	# remove empty parenthesis after an open cursor
	$str =~ s/(OPEN\s+[^\(\s;]+)\s*\(\s*\)/$1/isg;

	# replace is by for (some version for/is is both ok) 
	# while($str =~ s/(\bCURSOR\b\s*(.*)\s*)IS/$1FOR/is){};
    return $str;
}


=head2 rewrite_object_name
    checing for naming errors
    if the name is illegal , add double quote ;
=cut


sub rewrite_object_name
{
	my ($self, $obj_name) = @_;
    next if ($obj_name =~ /^SYS_NC\d+/);

    #remove any double quote and extra space
    $obj_name =~ s/"//g;
    $obj_name =~ s/^\s+//;
    $obj_name =~ s/\s+$//;

    # self->{lower_case} : attribute to lower obj_name
    if ($self->{lower_case})
    {
        $obj_name = lc($obj_name);
        # then if there is non alphanumeric or the object name is a reserved word
        if ($obj_name =~ /[^a-z0-9\_\.]/ || $self->search_key_words($obj_name) || $obj_name =~ /^\d+/)
        {
            # add double quote to [schema.] object name 
            if ($obj_name !~ /^[^\.]+\.[^\.]+$/ && $obj_name !~ /^[^\.]+\.[^\.]+\.[^\.]+$/) {
                $obj_name = '"' . $obj_name . '"';
            } elsif ($obj_name =~ /^[^\.]+\.[^\.]+$/) {
                $obj_name =~ s/^([^\.]+)\.([^\.]+)$/"$1"\."$2"/;
            } else {
                $obj_name =~ s/^([^\.]+)\.([^\.]+)\.([^\.]+)$/"$1"\."$2"\."$3"/;
            }
            $obj_name = '"' . $obj_name . '"' if ($obj_name =~ /^\d+/);
        }
    }
    # add double quote to [schema.] object name 
    elsif ($obj_name !~ /^[^\.]+\.[^\.]+$/ && $obj_name !~ /^[^\.]+\.[^\.]+\.[^\.]+$/) {
        $obj_name = "\"$obj_name\"";
    } elsif ($obj_name =~ /^[^\.]+\.[^\.]+$/) {
        $obj_name =~ s/^([^\.]+)\.([^\.]+)$/"$1"\."$2"/;
    } else {
        $obj_name =~ s/^([^\.]+)\.([^\.]+)\.([^\.]+)$/"$1"\."$2"\."$3"/;
    }
    return $obj_name;
}


=head2 convert_outer_join
    replace (+) with outer join 
=cut

sub conver_outer_join{
    my $str = shift;

    return shift;

}

=head2 remove_named_parameters
    used in convertion for  raise exception: remove some parameters 
=cut
sub remove_named_parameters
{
	my $str = shift;

	$str =~ s/\w+\s*=>\s*//g;

	return $str;
}

=head2 set_error_code
    set error code when conver raise exception ( now it isn't used)
=cut

sub set_error_code
{
	my $code = shift;

	my $orig_code = $code;

	$code =~ s/-20(\d{3})/'45$1'/;
	if ($code =~ s/-20(\d{2})/'450$1'/ || $code =~ s/-20(\d{1})/'4500$1'/) {
		print STDERR "WARNING: exception code has less than 5 digit, proceeding to automatic adjustement.\n";
		$code .= " /* code was: $orig_code */";
	}

	return $code;
}
